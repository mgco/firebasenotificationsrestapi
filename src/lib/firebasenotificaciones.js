var admin = require("firebase-admin");
const {JWT} = require('google-auth-library');
var request = require('request');

var serviceAccount = require("./bellpi-firebase-adminsdk-02tdt-04ab188c55");

//['https://www.googleapis.com/auth/cloud-platform'],

function getAccessToken() {
	

	app = admin.initializeApp({
	  credential: admin.credential.cert(serviceAccount),
	  databaseURL: "https://helpiapp-d3bcc.firebaseio.com"
	}, 'test');



	  return new Promise(function(resolve, reject) {
	    var key = require('./bellpi-firebase-adminsdk-02tdt-04ab188c55');
	    var jwtClient = new JWT(
	      key.client_email,
	      null,
	      key.private_key,
	      ['email','https://www.googleapis.com/auth/firebase.messaging '],
	      null
	    );
	    jwtClient.authorize(function(err, tokens) {
	      if (err) {

	        reject(resolve({'status': 'ERROR', 'data': err}));
	        return;
	      }
	      console.log(JSON.stringify(tokens));
	      resolve({'status': 'OK', 'data': tokens});
	    });
	  });
}


function _sendMessages(tokens, title, body) {
	console.log('tokens '+tokens);
	app = ""
	try {
		app = admin.initializeApp({
		  credential: admin.credential.cert(serviceAccount),
		  databaseURL: "https://helpiapp-d3bcc.firebaseio.com"
		}, 'name');
		console.log(app)

	} catch (err) {
		console.log(err);
	}



	var message = {
      tokens: tokens,
      "notification" : {
              "body" : body,
              "title" : title,
              }
    };

    return new Promise(function(resolve, reject) {
	    app.messaging().sendMulticast(message)
	      .then((response) => {
	        // Response is a message ID string.
	        // console.log('Successfully sent message:', response);
	        app.delete();
	        resolve({"status":"OK", "data": response })
	      })
	      .catch((error) => {
	        // console.log('Error sending message:', error);
	        app.delete();
	        reject({"status":"ERROR", "data": error })
	      });
	});
}

function _sendMessagesV2(tokens, title, body, app) {
	// console.log('tokens '+tokens);
	// console.log(app)
	// app = ""
	// try {
	// 	app = admin.initializeApp({
	// 	  credential: admin.credential.cert(serviceAccount),
	// 	  databaseURL: "https://helpiapp-d3bcc.firebaseio.com"
	// 	}, 'name');
	// 	console.log(app)

	// } catch (err) {
	// 	console.log(err);
	// }



	var message = {
      tokens: tokens,
      "notification" : {
              "body" : body,
              "title" : title,
              },
      "data" : {
              "body" : body,
              "title" : title,
              },
    };

    return new Promise(function(resolve, reject) {
	    app.messaging().sendMulticast(message)
	      .then((response) => {
	        // Response is a message ID string.
	        // console.log('Successfully sent message:', response);
	        // app.delete();
	        resolve({"status":"OK", "data": response })
	      })
	      .catch((error) => {
	        // console.log('Error sending message:', error);
	        // app.delete();
	        reject({"status":"ERROR", "data": error })
	      });
	});
}


var firebase = {
	getToken: async function(){
		return await getAccessToken();
	},
	sendMessages: async function(tokens, title, body){
		return await _sendMessages(tokens, title, body);
	},
	sendMessagesV2: async function(tokens, title, body,app){
		return await _sendMessagesV2(tokens, title, body, app);
	},
}


module.exports = firebase;
