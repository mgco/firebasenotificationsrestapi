const express = require('express');
const morgan = require('morgan');

const app = express()

//setttings
app.set('port', 3000);
app.set('json spaces', 2);

//middlewares
app.use(morgan('dev')); 

app.use(express.urlencoded({extended:false}));
app.use(express.json());

console.log('esto se levanta una sola vez')
//routes
app.use(require('./routes/index.js'));

app.listen(app.get('port'), () => {
	console.log(`diego Server on port ${app.get('port')}`);
})

