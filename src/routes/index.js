const Router = require('express');
const router = Router();
const firebase = require('../lib/firebasenotificaciones.js');



/****************************************************************/
var admin = require("firebase-admin");
var serviceAccount = require("../lib/bellpi-firebase-adminsdk-02tdt-04ab188c55");
app = admin.initializeApp({
		  credential: admin.credential.cert(serviceAccount),
		  databaseURL: "https://bellpi.firebaseio.com"
		}, 'name');
/****************************************************************/



router.set('api', '/api/v1/firebase/push/');

router.get( router.get('api') + 'get-token', async (req, res) => {
	const token = await firebase.getToken();
	res.json(token);
});

router.post( router.get('api') + 'send-messages', async (req, res) => {
	const {tokens, title, body} = req.body;
	// const response = await firebase.sendMessages(tokens, title, body);
	const response = await firebase.sendMessagesV2(tokens, title, body, app);
	res.json(response);
});

router.get( router.get('api') + 'send-messages', async (req, res) => {
	res.json("conexion al api exitosa por metodo GET");
});

module.exports = router;